#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

struct parent{
    double time[200];
};

int main(int argv, char *argc[]){
    FILE *fp,*outfile;
    struct parent p;
    char c[20],out[20];
    char filename[30],phase[20];
    int i=0;

    if (argv != 2){
        printf("format: hdf5_to_bin <path_to_ascii_file>\n");
        exit(0);
    }

    fp = fopen(argc[1],"r");
    strcat(argc[1],".bin");
    printf("%s\n",argc[1]);

    while (fgets(c,sizeof(c),fp) != NULL){
        if (i%2 == 0){
            puts(c);
            p.time[i] = atof(c);
        }
        i++;
    }
    outfile = fopen(argc[1],"wb");
    fwrite(&p,sizeof(struct parent),1,outfile); 
}
