#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#define c_dep 500
#define degree 101

struct reverb_lkup;

struct reverb_lkup{
    char name[30];
    float time[c_dep][degree];
    float dist[c_dep][degree];
};

int main(int argv, char *argc[]){

    if (argv != 2){
        printf("Usage: pierce_lkup <evdp>\n");
        exit(0);
    }
    FILE *fp;
    int h,j,k,i=0;
    const char *reverb[12];
    struct reverb_lkup r[12];
    char c[500],sa[3],sb[30],sc[1],sd[1],se[1],sf[1];
    float time, deg, dist, dep, sec;
    int depth,conv_depth;
    char *pch,*rch;

    h = atoi(argc[1]);
    //strcpy(fname,"60_400.gmt");

    reverb[0]  = "ScS^XScS";
    reverb[1]  = "ScS^XScSScS";
    reverb[2]  = "ScSScS^XScS";
    reverb[3]  = "sSvXSScS";
    reverb[4]  = "sScSSvXS";
    reverb[5]  = "sSvXSScSScS";
    reverb[6]  = "sScSSvXSScS";
    reverb[7]  = "sScSScSSvXS";
    reverb[8]  = "sSvXSScSScSScS";
    reverb[9]  = "sScSSvXSScSScS";
    reverb[10] = "sScSScSSvXSScS";
    reverb[11] = "sScSScSScSSvXS";

    for (i=0;i<12;i++){
        strcpy(r[i].name,reverb[i]);
    }

    for (i=400;i<415;i+=5){
        char fname[40]; 
        char numstr2[10];
        // find and open taup_pierce output
        //sprintf(numstr1,"%d",h);
        sprintf(numstr2,"%d",i);
        printf("%s\n",numstr2);
        strcat(fname, argc[1]);
        strcat(fname, "_");
        strcat(fname, numstr2);
        strcat(fname, ".gmt");
        printf("%s\n",fname);
            
        fp = fopen(fname,"r");
        while(fgets(c,sizeof(c),fp)){
            if (strstr(c,">")){
                // get extract time and st_gcarc
                sscanf(c,"%s %s %s %f %s %s %f %s",
                       sa,sb,sc,&time,sd,se,&deg,sf);
                for (i=0;i<12;i++){
                    pch = strstr(sb,"X");
                    //strncpy(pch,itoa(dep),3);
                    //if (pch == r[i].name){
                    //}
                }
                printf("%c\n",*sb+1);
            }
            else{
                sscanf(c,"%f %f %f",&dist,&dep,&sec);
                depth=(int)dep;
                //if (dep == conv_depth && dist > 0){
                    //printf("%s\n",sb);
                    //printf("%f %f %f\n",dist,dep,sec);

                //}
            }
        }

        fname[0] = '\0';
        fclose(fp);
    }
return 1;
}







