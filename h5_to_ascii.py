#!/home/samhaug/anaconda2/bin/python

'''
==============================================================================

File Name : h5_to_ascii.py
Purpose : ---
Creation Date : 03-10-2018
Last Modified : Fri 05 Oct 2018 03:26:27 PM EDT
Created By : Samuel M. Haugland

==============================================================================
'''

import numpy as np
import h5py

f = h5py.File('../synth_reverb.h5','r')

with open('reverb.txt','w') as t:
    for idx,keys in enumerate(f.keys()):
        t.write('NEW'+'\n')
        t.write(keys+'\n')
        np.savetxt(t,f[keys]['coords'][:],fmt='%8.3f')

        t.write('ScSScS\n')
        try:
            np.savetxt(t,f[keys]['ScSScS'][:],fmt='%8.8f')
        except KeyError:
            np.savetxt(t,np.zeros(5501),fmt='%8.8f')
        t.write('ScSScSScS\n')
        try:
            np.savetxt(t,f[keys]['ScSScSScS'][:],fmt='%8.8f')
        except KeyError:
            np.savetxt(t,np.zeros(5501),fmt='%8.8f')
        t.write('sScS\n')
        try:
            np.savetxt(t,f[keys]['sScS'][:],fmt='%8.8f')
        except KeyError:
            np.savetxt(t,np.zeros(5501),fmt='%8.8f')
        t.write('sScSScS\n')
        try:
            np.savetxt(t,f[keys]['sScSScS'][:],fmt='%8.8f')
        except KeyError:
            np.savetxt(t,np.zeros(5501),fmt='%8.8f')
        t.write('sScSScSScS\n')
        try:
            np.savetxt(t,f[keys]['sScSScSScS'][:],fmt='%8.8f')
        except KeyError:
            np.savetxt(t,np.zeros(5501),fmt='%8.8f')

